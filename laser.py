import pygame

class Bullet(pygame.sprite.Sprite):
	def __init__(self, x, y):
		pygame.sprite.Sprite.__init__(self)
		self.image  = pygame.Surface((20, 10))
		self.image.fill((240, 43, 12))
		self.rect = self.image.get_rect()
		self.rect.bottom = y
		self.rect.centerx = x
		self.speed = -10

	def update(self):
		self.rect.x += self.speed

		if self.rect.bottom < 0:
			self.kill()


class EnemyBullet(pygame.sprite.Sprite):
	def __init__(self, x, y):
		pygame.sprite.Sprite.__init__(self)
		self.image  = pygame.Surface((20, 10))
		self.image.fill((255, 201, 7))
		self.rect = self.image.get_rect()
		self.rect.bottom = y
		self.rect.centerx = x
		self.speed = 10

	def update(self):
		self.rect.y += self.speed

		if self.rect.bottom < 0:
			self.kill()

		
		