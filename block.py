import pygame as pg

class Block(pg.sprite.Sprite):
	def __init__(self, x, y):
		pg.sprite.Sprite.__init__(self)
		self.image = pg.Surface((32, 32))
		self.image.fill((0, 0, 0))
		self.rect = self.image.get_rect()
		self.rect.x = x
		self.rect.y = y
	