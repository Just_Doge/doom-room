import pygame

blockList = []


player_acc = 1.0
player_friction = -0.12
player_gravity = 0.5

enemy_acc = 1.5
enemy_friction = -0.11
enemy_gravity = 0.25

bullet_freq = 2000
jump_freq = 2000

bullets = pygame.sprite.Group()

true = True
false = False
width = 800
height = 600
FPS = 60