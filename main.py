

import pygame as pg
from player import *
from settings import *
from levels import *
from block import *
from enemy import *
from random import *
""" People who help solved problems on Stack Overflow:

 Right leg

 skrx """

class Game:
    def __init__(self):
        pg.init()
        pg.mixer.init()
        self.screen = pg.display.set_mode((width, height))
        pg.display.set_caption("doom room")
        self.clock = pg.time.Clock()
        self.enemiesList = []
        self.running = True
        self.shootRight = True
        self.cLevel = 0




    def loadLevel(self, level, enemies, group, group2, group3):

        for y in range(0, len(level)):
            for x in range(0, len(level[y])):
                if (level[y][x] == 1):
                    blockList.append(Block(x*32, y*32))
                    group.add(Block(x*32, y*32))
                    group2.add(Block(x*32, y*32))

        for index in range(0, enemies):
            print(levels[self.cLevel]["enemyList"][index])
            if levels[self.cLevel]["enemyList"][index] == "flying_enemy":
                enemy = FlyingEnemy(self)
                self.enemies.add(enemy)
                self.all_sprites.add(enemy)
            elif levels[self.cLevel]["enemyList"][index] == "flying_shooter":
                flying_shooter_enemy = FlyingShooter(self)
                self.enemies.add(flying_shooter_enemy)
                self.shootingEnemies.add(flying_shooter_enemy)
                self.all_sprites.add(flying_shooter_enemy)



    def new(self):

        self.platforms = pg.sprite.Group()
        self.all_sprites = pg.sprite.Group()
        self.enemies = pg.sprite.Group()
        self.bullets = pg.sprite.Group()
        self.enemyBullets = pg.sprite.Group()
        self.shootingEnemies = pg.sprite.Group()

        self.bullet_timer = 0

        self.player = Player()
        self.loadLevel(levels[self.cLevel]["platform"], levels[self.cLevel]["enemies"], self.platforms, self.all_sprites, self.enemies)
        self.all_sprites.add(self.player)
        self.run()


    def shoot(self):

        if self.shootRight:
            self.bullet = Bullet(self.player.rect.centerx, self.player.rect.centery)

            self.bullet.speed = 10
            self.all_sprites.add(self.bullet)
            self.bullets.add(self.bullet)
            print(self.bullet)
        elif self.shootRight == False:
            self.bullet = Bullet(self.player.rect.centerx, self.player.rect.centery)

            self.bullet.speed = -10
            self.all_sprites.add(self.bullet)
            self.bullets.add(self.bullet)
            print(self.bullets)

    def shootPlayer(self, enemy):
        self.enemyBullet = EnemyBullet(enemy.rect.centerx, enemy.rect.centery)

        self.enemyBullet.speed = 12
        self.enemyBullet.rect.y += self.enemyBullet.speed
        self.all_sprites.add(self.enemyBullet)
        self.enemyBullets.add(self.enemyBullet)



    def run(self):

        self.playing = True
        while self.playing:
            self.clock.tick(FPS)
            self.events()
            self.update()
            self.draw()

    def update(self):
        self.all_sprites.update()
        for sprite in self.all_sprites:
            sprite.update()

        now = pg.time.get_ticks()
        if now - self.bullet_timer > bullet_freq - choice([500, 300, 200, 100, 800]):
            for shooter in self.shootingEnemies:
                self.bullet_timer = now
                self.shootPlayer(shooter)


        self.enemy_hits = pg.sprite.spritecollide(self.player, self.enemies, False)
        self.laser_hits = pg.sprite.groupcollide(self.enemyBullets, self.platforms, True, False)
        self.player_laser_hits = pg.sprite.groupcollide(self.bullets, self.platforms, True, False)

        self.hit_player = pg.sprite.spritecollide(self.player, self.enemyBullets, False)
        if self.hit_player:
            self.show_go_screen()
        #print(enemy_hits)
        if self.enemy_hits:
            self.show_go_screen()
            #print("hit")
        for enemy in pygame.sprite.groupcollide(self.enemies,self.bullets, True, True).keys():
            print("complete")
            enemy.kill()
            levels[self.cLevel]["enemies"] -= 1
        if levels[self.cLevel]["enemies"] <= 0:
            self.cLevel += 1
            for sprite in self.all_sprites:
                sprite.kill()
            self.new()



        # if self.bullet_hits:
        # 	print("w")
        # 	print("o")
        # 	print("r")
        # 	print("k")
        # 	print("i")
        # 	print("n")
        # 	print("g")
        # 	print("")
        # 	print("")
        # 	print("")
        # 	print("")
        # 	print("")




        hits = pg.sprite.spritecollide(self.player, self.platforms, False)

        if hits:
            self.player.pos.y = hits[0].rect.top + 1
            self.player.vel.y = 0

    def events(self):
        for event in pg.event.get():
            if event.type == pg.QUIT:
                if self.playing:
                    self.playing = False
                    self.running = false
            if event.type == pg.KEYDOWN:
                if event.key == pg.K_UP:
                    self.player.jump()
                if event.key == pg.K_SPACE:
                    self.shoot()
                if event.key == pg.K_RIGHT:
                    self.shootRight = True
                if event.key == pg.K_LEFT:
                    self.shootRight = False





    def draw(self):
        self.screen.fill((255, 255, 255))
        self.all_sprites.draw(self.screen)
        pg.display.flip()

    def show_start_screen(self):
        self.screen.fill((255, 255, 255))
        self.draw_text("doom room.", 50, (0, 0, 0), width / 2, height / 2)
        self.draw_text("use arrows to move and aim and space bar to shoot.", 25, (0, 0, 0), width / 2, height - height/4 )
        self.draw_text("press any key to start.", 25, (0, 0, 0), width / 2, height / 4)
        pg.display.flip()
        self.wait_for_key()
    def show_go_screen(self):
        self.cLevel = 0
        self.screen.fill((255, 255, 255))
        self.draw_text("u dea'.", 50, (0, 0, 0), width / 2, height / 2)
        self.draw_text("press a key to try again.", 25, (0, 0, 0), width / 2, height - height/4 )
        pg.display.flip()
        self.wait_for_key()

    def wait_for_key(self):
        waiting = True
        while waiting:
            self.clock.tick(FPS)
            for event in pg.event.get():
                if event.type == pg.QUIT:
                    waiting = False
                    self.running = False
                    pygame.quit()
                if event.type == pg.KEYUP:
                    waiting = false

    def draw_text(self, text, size, color, x, y):
        font = pg.font.SysFont("Futura", size)
        text_surface = font.render(text, True, color)
        text_rect = text_surface.get_rect()
        text_rect.midtop = (x, y)
        self.screen.blit(text_surface, text_rect)

g = Game()
g.show_start_screen()
while g.running:
    g.new()

pg.quit()


width = 800
height = 600
FPS = 60

pg.init()
pg.mixer.init()
screen = pg.display.set_mode((width, height))
pg.display.set_caption("doom room")
clock = pg.time.Clock()


running = True

while running:
    for event in pg.event.get():
        clock.tick(FPS)
        if event.type == pg.QUIT:
            running = false

    screen.fill((255, 255, 255))
    pg.display.flip()

pg.quit()
