import pygame as pg
from random import *
from settings import *

vec = pg.math.Vector2

class FlyingEnemy(pg.sprite.Sprite):
	def __init__(self, game):
		pg.sprite.Sprite.__init__(self)
		self.game = game
		self.image = pg.Surface((45, 45))
		self.image.fill((20, 203, 50))
		self.rect = self.image.get_rect()
		self.rect.centerx = choice([-100, width + 100])
		self.vx = randrange(1, 10)
		if self.rect.centerx > width:
			self.vx *= -1
		self.rect.y = height / 4

		self.rect.x = 0
		self.vy = 0
		self.dy = 0.5



	def update(self):
		if self.rect.x > width:
			self.rect.x = 0
		if self.rect.x < 0:
			self.rect.x = width

		self.rect.x += self.vx
		self.vy += self.dy
		if self.vy > 3 or self.vy < -3:
			self.dy *= -1
		center = self.rect.center
		if self.dy < 0:
			pass
			#print("bobbed up")
		else:
			pass
			#print("bobbed down")


class FlyingShooter(FlyingEnemy):
	def __init__(self, game):
		FlyingEnemy.__init__(self, game)
		self.image.fill((203, 20, 50))
		self.rect = self.image.get_rect()
		self.rect.centerx = choice([-100, width + 100])
		self.vx = randrange(4, 7)
		if self.rect.centerx > width:
			self.vx *= -1
		self.rect.y = height / 4
		self.rect.x = 0
		self.vy = 0
		self.dy = 0.5




