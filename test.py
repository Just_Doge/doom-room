import pygame as pg
from random import *
from settings import *

pg.init()

running = True
screen = pg.display.set_mode((width, height))
clock = pg.time.Clock()


enemyGroup = pg.sprite.Group()



allGroup = pg.sprite.Group()
bullets = pg.sprite.Group()


class Bullet(pygame.sprite.Sprite):
	def __init__(self, x, y):
		pygame.sprite.Sprite.__init__(self)
		self.image  = pygame.Surface((20, 10))
		self.image.fill((240, 43, 12))
		self.rect = self.image.get_rect()
		self.rect.bottom = y
		self.rect.centerx = x
		self.speed = -10

	def update(self):
		self.rect.x += self.speed

		if self.rect.bottom < 0:
			self.kill()



class FlyingEnemy(pg.sprite.Sprite):
	def __init__(self):
		pg.sprite.Sprite.__init__(self)
		self.image = pg.Surface((45, 45))
		self.image.fill((20, 203, 50))
		self.rect = self.image.get_rect()
		self.rect.centerx = choice([-100, width + 100])
		self.vx = randrange(4, 7)
		if self.rect.centerx > width:
			self.vx *= -1
		self.rect.y = height / 4
		self.rect.x = 0
		self.vy = 0
		self.dy = 0.5



	def update(self):
		if self.rect.x > width:
			self.rect.x = 0
		if self.rect.x < 0:
			self.rect.x = width

		self.rect.x += self.vx
		self.vy += self.dy
		if self.vy > 3 or self.vy < -3:
			self.dy *= -1
		center = self.rect.center
		if self.dy < 0:
			pass
			#print("bobbed up")
		else:
			pass
			#print("bobbed down")


from settings import *
vec = pg.math.Vector2

class Player(pg.sprite.Sprite):
	def __init__(self):
		pg.sprite.Sprite.__init__(self)
		self.image = pg.Surface((40, 40))
		self.image.fill((80, 123, 255))
		self.rect = self.image.get_rect()
		self.rect.center = (width / 2, height / 2)
		#self.vx = 0
		#self.vy = 0

	def jump(self):
		self.vel.y = -15

	def update(self):
		
		keys = pg.key.get_pressed()
		if keys[pg.K_LEFT]:
			self.rect.move_ip(-5, 0)
		if keys[pg.K_RIGHT]:
			self.rect.move_ip(5, 0)
		if keys[pg.K_UP]:
			self.rect.move_ip(0, -5)
		if keys[pg.K_DOWN]:
			self.rect.move_ip(0, 5)

	def shoot(self):
		bullet = Bullet(self.rect.centerx, self.rect.top)
		allGroup.add(bullet)
		bullets.add(bullet)


		
	

player = Player()
enemy = FlyingEnemy()
enemy2 = FlyingEnemy()

enemyGroup.add(enemy)
enemyGroup.add(enemy2)


allGroup.add(enemy)
allGroup.add(enemy)
allGroup.add(player)
while running:
	
	for event in pg.event.get():
		if event.type == pg.QUIT:
			running = False

		elif event.type == pg.KEYDOWN:
			if event.key == pg.K_SPACE:
				player.shoot()
				


	enemy_hits = pg.sprite.spritecollide(player, enemyGroup, False)
	print(enemy_hits)
	if enemy_hits:
		print("hit")
		#running = false
	bullet_hits = pg.sprite.groupcollide(enemyGroup, bullets, True, True)

	screen.fill((255, 255, 255))
	allGroup.draw(screen)
	allGroup.update()
	clock.tick(FPS)
	pg.display.flip()

pg.quit()



