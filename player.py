import pygame as pg
from settings import *
from laser import *

vec = pg.math.Vector2

class Player(pg.sprite.Sprite):
    def __init__(self):
        pg.sprite.Sprite.__init__(self)
        self.image = pg.Surface((40, 40))
        self.image.fill((80, 123, 255))
        self.rect = self.image.get_rect()
        self.rect.center = (width / 2, height / 2)
        self.pos = vec(width / 2, height / 2)
        self.vel = vec(0, 0)
        self.acc = vec(0, 0)

    def jump(self):
        self.vel.y = -15



    def update(self):

        self.acc = vec(0, player_gravity)
        keys = pg.key.get_pressed()
        if keys[pg.K_LEFT]:
            print("moving left")
            self.acc.x = -player_acc
        if keys[pg.K_RIGHT]:
            print("moving right")
            self.acc.x = player_acc

        self.acc.x += self.vel.x * player_friction
        self.vel += self.acc
        self.pos += self.vel + 0.5 * self.acc
        print("x acceleration: {0} velocity: {1} position: {2}".format(self.acc.x, self.vel, self.pos))
        if self.pos.x > width:
            self.pos.x = 0
        if self.pos.x < 0:
            self.pos.x = width
        if self.pos.y <= 0:
            self.pos.y += 15
